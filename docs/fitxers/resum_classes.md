## Quadre resum de classes relacionades amb fitxers


| Ús | Llegir text | Escriure text | Llegir binari | Escriure binari |
| -- |
| Classe abstracte base de les altres | *Reader* | *Writer* | *InputStream* | *OutputStream* |
| Bytes/caràcters individuals, o grups de bytes/caràcters. Classe principal per accedir a fitxers. | *FileReader* | *FileWriter* | *FileInputStream* | *FileOutputStream* |
| Dades simples (int, double, cadenes...) | - | - | *DataInputStream* | *DataOutputStream* |
| Buffers intermedis per reduir la quantitat d'accesssos a disc. Els de text permeten treballar línia a línia. | *BufferedReader* | *BufferedWriter* | *BufferedInputStream* | *BufferedOutputStream*
| Sortida amb format | - | *PrintWriter*\* | - | *PrintStream*\* |
| Per objectes | - | - | *ObjectInputStream* | *ObjectOutputStream* |
| Transforma un flux d'un tipus en un altre | *InputStreamReader* (d'*InputStream* a *Reader*) | *OutputStreamWriter* (d'*OutputStream* a *Writer*) | - | - |

\*: *PrintWriter* i *PrintStream* disposen entre d'altres dels mètodes *format*,
*println* que estem acostumats a utilitzar en un *Scanner*.

Són útils per escriure text quan necessitem donar-li un format complex, però
tenen un inconvenient important: no llancen excepcions. En comptes d'això,
cal cridar un mètode anomenat *checkError()* per comprovar si s'ha produït un
error. Això dificulta la creació de programes robustos i per això no es
recomana el seu ús en general.

La diferència principal entre *PrintWriter* i *PrintStream* és que
*PrintStream* escriu sempre en la codificació de caràcters per defecte del
sistema, mentre que amb *PrintWriter* la podem triar.
