#### Diccionaris

Un **diccionari**, també anomenat **array associatiu** o **mapa**, és una
estructura dinàmica composta d'una col·lecció de **claus** úniques i una
col·lecció de **valors** tals que a cada clau s'associa un valor.

Els diccionaris són una generalització del concepte de vector. Podem
veure un vector com un diccionari que associa els primers enters a una
sèrie de valors. En el cas dels diccionaris les claus no tenen perquè
ésser enters consecutius, ni tan sols han d'ésser enters. Les claus han
de ser úniques i apuntar a un únic valor (però res impedeix que aquest
valor pugui ser una llista o qualsevol altra estructura).

Exemples de diccionaris:

 * Una agenda que associa a cada nom un telèfon. Els noms són les claus
 i els telèfons són els valors.

 * Un diccionari de paraules: a cada paraula s'associa la seva definició.
 Les paraules són les claus i les definicions els valors.

 * Una colla de països associats a la seva capital. Els noms dels països
 són les claus i els noms de les capitals els valors.

 * Funcions matemàtiques amb un nombre finit de termes. Per exemple, el
 factorial des de l'1 fins al 100.

---

 * [Definició d'un diccionari](definicio.md)
 * [Operacions sobre diccionaris](operacions.md)
